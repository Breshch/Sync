﻿using Newtonsoft.Json;
using Sync;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace Synchronizer
{
    class Program
    {
        public const string _FTP = "ftp://91.77.166.229";

        public const string _PATHTODICTFTP = _FTP + "/Natacha_Bacs/Dictionary.txt";
        public const string _PATHTOROOT = _FTP + "/Natacha_Bacs";
        public const string _PATHTOFTPDELETEDBACK = _FTP + "/Natacha_Bacs/DeletedFiles";
        public static string _PATHTOLOCAL;

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int _SW_HIDE = 0;
        //const int _SW_SHOW = 5;

        public static List<string> _EXEPTIONLIST = new List<string>
        {
            "$RECYCLE.BIN",
            "msdownld.tmp",
            "System Volume Information",
            ".dropbox.device",
            "DeletedFiles"
            //"Химэксперт"

        };

        static void Main(string[] args)
        {
            Dictionary<string, ValueInfo> values = new Dictionary<string, ValueInfo>();

            var handle = GetConsoleWindow();

            // Hide
            ShowWindow(handle, _SW_HIDE);

            // Show
            // ShowWindow(handle, _SW_SHOW);

            DownloadFtp(_PATHTODICTFTP);
            var oldValues = ReadFile();

            var rootDir = GetDisc();
            _PATHTOLOCAL = rootDir.FullName;
            values = GetTree(rootDir, values);
            Compare(values, oldValues);
            SaveToFile(values);

            //Console.ReadKey();//для дебага

        }

        public static void Compare(Dictionary<string, ValueInfo> newDicts, Dictionary<string, ValueInfo> oldDicts)
        {
            int count = newDicts.Count;
            //File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt"), JsonConvert.SerializeObject(newDicts));
            foreach (var newDict in newDicts)
            {
                if (oldDicts.ContainsKey(newDict.Key))
                {
                    var oldHash = oldDicts[newDict.Key].Hash;
                    if (newDict.Value.Hash != oldHash)
                    {
                        FtpRemove(Path.Combine(_PATHTOROOT, newDict.Key));
                        UploadFTPResave(Path.Combine(_PATHTOLOCAL, newDict.Key), Path.Combine(_PATHTOROOT, newDict.Key), newDict.Value.IsFolder);
                    }
                }
                else
                {
                    if (!_EXEPTIONLIST.Contains(newDict.Key))
                    {
                        UploadFTPNew(newDict.Key, newDict.Value.IsFolder);
                        count--;
                        Console.WriteLine(count);
                    }

                }
            }
            foreach (var oldDict in oldDicts)
            {
                if (!newDicts.ContainsKey(oldDict.Key))
                {
                    FtpMoveFiles(Path.Combine(_PATHTOROOT, oldDict.Key), _PATHTOFTPDELETEDBACK, oldDict.Value.IsFolder);

                }
            }
        }

        public static void FtpMoveFiles(string source, string destination, bool isFolder)
        {
            var request = MakeFtpWebRequest(source, WebRequestMethods.Ftp.DownloadFile);
            string path = source.Substring(33);
            path = path.Replace('\\', '?');
            if (!isFolder)
            {
                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(Path.Combine(_PATHTOLOCAL, "temp")))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                        Console.WriteLine(source + ": ", "Downloaded {0} bytes", fileStream.Position);
                    }
                }

                request = MakeFtpWebRequest(Path.Combine(destination, path), WebRequestMethods.Ftp.UploadFile);
                using (Stream fileStream = File.OpenRead(Path.Combine(_PATHTOLOCAL, "temp")))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine(destination + ": ", "Uploaded {0} bytes", fileStream.Position);
                    }
                }

                if (File.Exists(Path.Combine(_PATHTOLOCAL, "temp")))
                {
                    File.Delete(Path.Combine(_PATHTOLOCAL, "temp"));
                }
            }
        }



        public static void SaveToFile(Dictionary<string, ValueInfo> values)
        {
            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt"), JsonConvert.SerializeObject(values));//можно убрать
            if (IsExist(_PATHTODICTFTP))
            {
                FtpRemove(_PATHTODICTFTP);
            }
            UploadFTPDict();
        }

        public static void FtpRemove(string path)
        {
            var request = MakeFtpWebRequest(path, WebRequestMethods.Ftp.DeleteFile);
            using ((FtpWebResponse)request.GetResponse()) ;
        }

        public static bool IsExist(string path)
        {
            var request = MakeFtpWebRequest(path, WebRequestMethods.Ftp.DownloadFile);
            request.UseBinary = true;
            try
            {
                using ((FtpWebResponse)request.GetResponse()) ;
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                string stat = ((FtpWebResponse)ex.Response).StatusDescription;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }

        public static Dictionary<string, ValueInfo> ReadFile()
        {
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, ValueInfo>> 
                (File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "OldDictionary.txt")));
            return dictionary;
        }

        public static void UploadFTPDict()
        {
            var request = MakeFtpWebRequest(_PATHTODICTFTP, WebRequestMethods.Ftp.UploadFile);
            {
                using (Stream fileStream = File.OpenRead(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt")))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine(_PATHTODICTFTP + ": Uploaded {0} bytes", fileStream.Position);
                    }
                }
            }
        }

        public static void UploadFTPResave(string pathToLocal, string pathToFtp, bool isFolder)
        {
            if (isFolder)
            {
                var request = MakeFtpWebRequest(pathToFtp, WebRequestMethods.Ftp.MakeDirectory);
                using ((FtpWebResponse)request.GetResponse()) ;
            }
            else
            {
                var request = MakeFtpWebRequest(pathToFtp, WebRequestMethods.Ftp.UploadFile);
                using (Stream fileStream = File.OpenRead(pathToLocal))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine(pathToFtp + ": ", "Uploaded {0} bytes ", fileStream.Position);
                    }
                }
            }
        }

        public static void UploadFTPNew(string path, bool isFolder)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Path.Combine(_PATHTOROOT, path));
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");

            if (isFolder)
            {
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                using ((FtpWebResponse)request.GetResponse()) ;
                Console.WriteLine("Maked directory :" + path);
            }
            else
            {
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                using (Stream fileStream = File.OpenRead(Path.Combine(_PATHTOLOCAL, path)))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine("Uploaded {0} bytes ", fileStream.Position + " : -->" + path);
                    }
                }
            }
        }


        public static void DownloadFtp(string path)
        {
            var request = MakeFtpWebRequest(path, WebRequestMethods.Ftp.DownloadFile);
            using (Stream ftpStream = request.GetResponse().GetResponseStream())
            using (Stream fileStream = File.Create(Path.Combine(Environment.CurrentDirectory, "OldDictionary.txt")))
            {
                byte[] buffer = new byte[10240];
                int read;
                while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, read);
                    Console.WriteLine(path + ": ", "Downloaded {0} bytes", fileStream.Position);
                }
            }
        }

        private static Dictionary<string, ValueInfo> GetTree(DirectoryInfo root, Dictionary<string, ValueInfo> values)
        {
            try
            {
                string hash;
                FileInfo[] files = null;
                DirectoryInfo[] subDirs = null;

                try
                {
                    files = root.GetFiles("*.*");
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message);
                }

                catch (DirectoryNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
                if (files != null)
                {
                    foreach (var fi in files)
                    {
                        hash = Hash(fi.Length.ToString());
                        var valueInfo = new ValueInfo { Hash = hash, IsFolder = false };
                        values.Add(fi.FullName.Substring(3), valueInfo);
                    }
                    subDirs = root.GetDirectories();
                    foreach (var dirInfo in subDirs)
                    {
                        if (!_EXEPTIONLIST.Contains(dirInfo.Name))
                        {
                            hash = Hash(dirInfo.ToString());
                            var valueInfo = new ValueInfo { Hash = hash, IsFolder = true };
                            values.Add(dirInfo.FullName.Substring(3), valueInfo);
                            GetTree(dirInfo, values);
                        }
                    }
                    return values;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
            return values;
        }


        private static string Hash(string input)
        {
            var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Concat(hash.Select(b => b.ToString("x2")));
        }

        private static string GetNameOfLookingFolder()
        {
            foreach (string key in ConfigurationManager.AppSettings)
            {
                //Get your key
                string value = ConfigurationManager.AppSettings[key]; 
                return value;
            }

            return "";
        }

        private static DirectoryInfo GetDisc()
        {
            var discs = Environment.GetLogicalDrives();
            string nameOfLookingFolder = GetNameOfLookingFolder();
            foreach (var disc in discs)
            {
                DirectoryInfo path = new DirectoryInfo(disc);
                var dirs = path.GetDirectories();
                if (dirs.Any(x => x.Name == nameOfLookingFolder))
                {
                    return path;
                }
            }
            throw new Exception("Нет диска");
        }

        private static FtpWebRequest MakeFtpWebRequest(string path, string method)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.Method = method;
            request.UsePassive = false;
            return request;
        }
    }
}
