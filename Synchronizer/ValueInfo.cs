﻿namespace Sync
{
    public class ValueInfo
    {
        public string Hash { get; set; }
        public bool IsFolder { get; set; }
    }
}
