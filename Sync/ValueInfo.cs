﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync
{
    public class ValueInfo
    {
        public string Hash { get; set; }
        public bool IsFolder { get; set; }
    }
}
