﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Sync
{
    public partial class Sync : Form
    {
        public Sync()
        {
            InitializeComponent();
        }
        public const string _PATHTODICTFTP = "ftp://91.77.166.229/Natacha_Bacs/Dictionary.txt";
        public const string _PATHTOROOT = "ftp://91.77.166.229/Natacha_Bacs";
        public string _PATHTOLOCAL;
        public const string _PATHTOFTPDELETEDBACK = "ftp://91.77.166.229/Natacha_Bacs/DeletedFiles";
       
        public List<string> _EXEPTIONLIST = new List<string>
        {
            "$RECYCLE.BIN",
            "msdownld.tmp",
            "System Volume Information",
            ".dropbox.device",
            "DeletedFiles"

        };

        public void Sync_Load(object sender, EventArgs e)
        {
            Dictionary<string, ValueInfo> values = new Dictionary<string, ValueInfo>();

            label_Last_Update_Time.Text = CheckDateFromFtp(_PATHTODICTFTP);
            DownloadFtp(_PATHTODICTFTP);
            var oldValues = ReadFile();
            var rootDir = GetDisc();
            _PATHTOLOCAL = rootDir.FullName;
            values = GetTree(rootDir, values);
            Compare(values, oldValues);
            SaveToFile(values);
        }

        public void Compare(Dictionary<string, ValueInfo> newDicts, Dictionary<string, ValueInfo> oldDicts)
        {

            foreach (var newDict in newDicts)
            {
                if (oldDicts.ContainsKey(newDict.Key))
                {
                    var oldHash = oldDicts[newDict.Key].Hash;
                    if (newDict.Value.Hash != oldHash)
                    {
                        FtpRemove(Path.Combine(_PATHTOROOT, newDict.Key));
                        UploadFTPResave(Path.Combine(_PATHTOLOCAL, newDict.Key), Path.Combine(_PATHTOROOT, newDict.Key), newDict.Value.IsFolder);
                    }
                }
                else
                {
                    UploadFTPNew(newDict.Key, newDict.Value.IsFolder);
                }
            }
            foreach (var oldDict in oldDicts)
            {
                if (!newDicts.ContainsKey(oldDict.Key))
                {
                    FtpMoveFiles(Path.Combine(_PATHTOROOT, oldDict.Key), _PATHTOFTPDELETEDBACK, oldDict.Value.IsFolder);

                }
            }
        }

        public void FtpMoveFiles(string source, string destination, bool isFolder)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(source);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            string path = source.Substring(33);
            path = path.Replace('\\', '?');
            if (!isFolder)
            {
                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(Path.Combine(_PATHTOLOCAL, "temp")))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                        Console.WriteLine("Downloaded {0} bytes", fileStream.Position);
                    }
                }


                request = (FtpWebRequest)WebRequest.Create(Path.Combine(destination, path));
                request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
                request.Method = WebRequestMethods.Ftp.UploadFile;

                using (Stream fileStream = File.OpenRead(Path.Combine(_PATHTOLOCAL, "temp")))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine("Uploaded {0} bytes", fileStream.Position);
                    }
                }

                if (File.Exists(Path.Combine(_PATHTOLOCAL, "temp")))
                {
                    File.Delete(Path.Combine(_PATHTOLOCAL, "temp"));
                }
            }
        }



        public void SaveToFile(Dictionary<string, ValueInfo> values)
        {
            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt"), JsonConvert.SerializeObject(values));//можно убрать
            if (IsExist(_PATHTODICTFTP))
            {
                FtpRemove(_PATHTODICTFTP);
            }
            UploadFTPDict();
        }

        public void FtpRemove(string path)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            using ((FtpWebResponse)request.GetResponse()) ;
        }

        public void FtpMakeNew(string path)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.Method = WebRequestMethods.Ftp.UploadFile;
            using (Stream fileStream = File.OpenRead(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt")))
            using (Stream ftpStream = request.GetRequestStream())
            {
                byte[] buffer = new byte[10240];
                int read;
                while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ftpStream.Write(buffer, 0, read);
                    Console.WriteLine("Uploaded {0} bytes", fileStream.Position);
                }
            }
        }

        public bool IsExist(string path)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.UseBinary = true;
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            try
            {
                using ((FtpWebResponse)request.GetResponse()) ;
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                string stat = ((FtpWebResponse)ex.Response).StatusDescription;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }

        public Dictionary<string, ValueInfo> ReadFile()
        {
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, ValueInfo>>
                (File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "OldDictionary.txt")));
            return dictionary;
        }

        public string CheckDateFromFtp(string path)
        {
            if (IsExist(path))
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
                request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
                request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                using (FtpWebResponse resp = (FtpWebResponse)request.GetResponse())
                {
                    var lastModified = resp.LastModified;
                    return lastModified.ToString();
                }
            }


            FtpMakeNew(path);
            return _PATHTODICTFTP;
        }

        public void UploadFTPDict()
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(_PATHTODICTFTP);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            {
                request.Method = WebRequestMethods.Ftp.UploadFile;
                using (Stream fileStream = File.OpenRead(Path.Combine(Environment.CurrentDirectory, "Dictionary.txt")))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine("Uploaded {0} bytes", fileStream.Position);
                    }
                }
            }
        }

        public void UploadFTPResave(string pathToLocal, string pathToFtp, bool isFolder)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(pathToFtp);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");

            if (isFolder)
            {
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                using ((FtpWebResponse)request.GetResponse()) ;
            }
            else
            {
                request.Method = WebRequestMethods.Ftp.UploadFile;
                using (Stream fileStream = File.OpenRead(pathToLocal))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine("Uploaded {0} bytes", fileStream.Position);
                    }
                }
            }
        }

        public void UploadFTPNew(string path, bool isFolder)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Path.Combine(_PATHTOROOT, path));
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");

            if (isFolder)
            {
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                using ((FtpWebResponse)request.GetResponse()) ;
            }
            else
            {
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                using (Stream fileStream = File.OpenRead(Path.Combine(_PATHTOLOCAL, path)))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ftpStream.Write(buffer, 0, read);
                        Console.WriteLine("Uploaded {0} bytes", fileStream.Position);
                    }
                }
            }
        }

        public void DownloadFtp(string path)
        {
            FtpWebRequest request =
                (FtpWebRequest)WebRequest.Create(path);
            request.Credentials = new NetworkCredential("admin", "Mp72-RnAs-00aA");
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (Stream ftpStream = request.GetResponse().GetResponseStream())
            using (Stream fileStream = File.Create(Path.Combine(Environment.CurrentDirectory, "OldDictionary.txt")))
            {
                byte[] buffer = new byte[10240];
                int read;
                while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, read);
                    Console.WriteLine("Downloaded {0} bytes", fileStream.Position);
                }
            }
        }

        private Dictionary<string, ValueInfo> GetTree(DirectoryInfo root, Dictionary<string, ValueInfo> values)
        {
            try
            {
                string hash;
                FileInfo[] files = null;
                DirectoryInfo[] subDirs = null;

                try
                {
                    files = root.GetFiles("*.*");
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message);
                }

                catch (DirectoryNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
                if (files != null)
                {
                    foreach (var fi in files)
                    {

                        hash = Hash(fi.Length.ToString());
                        var valueInfo = new ValueInfo { Hash = hash, IsFolder = false };
                        values.Add(fi.FullName.Substring(3), valueInfo);
                    }
                    subDirs = root.GetDirectories();
                    foreach (var dirInfo in subDirs)
                    {
                        if (!_EXEPTIONLIST.Contains(dirInfo.Name))
                        {
                            hash = Hash(dirInfo.ToString());
                            var valueInfo = new ValueInfo { Hash = hash, IsFolder = true };
                            values.Add(dirInfo.FullName.Substring(3), valueInfo);
                            GetTree(dirInfo, values);
                        }


                    }
                    return values;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
            return values;
        }


        static string Hash(string input)
        {
            var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Concat(hash.Select(b => b.ToString("x2")));
        }

        public DirectoryInfo GetDisc()
        {
            var discs = Environment.GetLogicalDrives();
            foreach (var disc in discs)
            {
                DirectoryInfo path = new DirectoryInfo(disc);
                var dirs = path.GetDirectories();
                if (dirs.Any(x => x.Name == "Dev"))
                {
                    return path;
                }

            }

            throw new Exception("Нет диска");
        }

        public void button_Select_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string folder = dlg.SelectedPath;
                DriveInfo di = new DriveInfo(folder);
                DirectoryInfo rootDir = di.RootDirectory;
                string rootDirStr = rootDir.ToString();

            }
            else
            {

            }
        }
    }
}





