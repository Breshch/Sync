﻿
namespace Sync
{
    partial class Sync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label_Last_Update_Time = new System.Windows.Forms.Label();
            this.button_Update = new System.Windows.Forms.Button();
            this.label_Select_Drive = new System.Windows.Forms.Label();
            this.button_Select = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Last update time:";
            // 
            // label_Last_Update_Time
            // 
            this.label_Last_Update_Time.AutoSize = true;
            this.label_Last_Update_Time.Location = new System.Drawing.Point(106, 27);
            this.label_Last_Update_Time.Name = "label_Last_Update_Time";
            this.label_Last_Update_Time.Size = new System.Drawing.Size(0, 13);
            this.label_Last_Update_Time.TabIndex = 1;
            // 
            // button_Update
            // 
            this.button_Update.Location = new System.Drawing.Point(15, 152);
            this.button_Update.Name = "button_Update";
            this.button_Update.Size = new System.Drawing.Size(75, 23);
            this.button_Update.TabIndex = 2;
            this.button_Update.Text = "Update";
            this.button_Update.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_Update.UseVisualStyleBackColor = true;
            // 
            // label_Select_Drive
            // 
            this.label_Select_Drive.AutoSize = true;
            this.label_Select_Drive.Location = new System.Drawing.Point(12, 66);
            this.label_Select_Drive.Name = "label_Select_Drive";
            this.label_Select_Drive.Size = new System.Drawing.Size(65, 13);
            this.label_Select_Drive.TabIndex = 4;
            this.label_Select_Drive.Text = "Select Drive";
            // 
            // button_Select
            // 
            this.button_Select.Location = new System.Drawing.Point(15, 119);
            this.button_Select.Name = "button_Select";
            this.button_Select.Size = new System.Drawing.Size(75, 23);
            this.button_Select.TabIndex = 5;
            this.button_Select.Text = "Select drive";
            this.button_Select.UseVisualStyleBackColor = true;
            // 
            // Sync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 230);
            this.Controls.Add(this.button_Select);
            this.Controls.Add(this.label_Select_Drive);
            this.Controls.Add(this.button_Update);
            this.Controls.Add(this.label_Last_Update_Time);
            this.Controls.Add(this.label1);
            this.Name = "Sync";
            this.Text = "Sync";
            this.Load += new System.EventHandler(this.Sync_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_Last_Update_Time;
        private System.Windows.Forms.Button button_Update;
        private System.Windows.Forms.Label label_Select_Drive;
        private System.Windows.Forms.Button button_Select;
    }
}

