﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace Sync
{
    public class FtpItem
    {
        public char[] Permissions { get; set; }
        public int Size { get; set; }
        public DateTime LastModified { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public bool IsDirectory { get; set; }
        public int NrOfInodes { get; set; }
        public string User { get; set; }
        public string Group { get; set; }
        public int FileSize { get; set; }
        public string FileName { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class FtpDirectory : FtpItem
    {
        public FtpDirectory(FtpItem item)
        {
            Permissions = item.Permissions;
            Size = item.Size;
            LastModified = item.LastModified;
            Name = item.Name;
            FullPath = item.FullPath;
        }

        public List<FtpItem> SubItems { get; set; }
    }

    public class FtpFile : FtpItem
    {
        public FtpFile(FtpItem item)
        {
            Permissions = item.Permissions;
            Size = item.Size;
            LastModified = item.LastModified;
            Name = item.Name;
            FullPath = item.FullPath;
        }
    }

    public class FtpClient
    {
        private readonly string _address;
        private readonly string _username;
        private readonly string _password;

        public FtpClient(string address, string username, string password)
        {
            _address = address.StartsWith("ftp://", StringComparison.OrdinalIgnoreCase) ? address : $"ftp://{address}";
            _username = username;
            _password = password;
        }

        private protected static Regex m_FtpListingRegex = new Regex(
            @"^([d-])((?:[rwxt-]{3}){3})\s+(\d{1,})\s+(\w+)?\s+(\w+)?\s+(\d{1,})\s+(\w+)\s+(\d{1,2})\s+(\d{4})?(\d{1,2}:\d{2})?\s+(.+?)\s?$",
            RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        private protected static readonly string TimeFormat = "MMM dd yyyy HH:mm";

        public List<FtpItem> RetrieveDirectoryListingsFromFtp(string startingPath = null)
        {
            List<FtpItem> results = new List<FtpItem>();
            string path = !string.IsNullOrEmpty(startingPath) ? startingPath.Replace(" ", "%20") : _address;
            path = !path.StartsWith(_address) ? $"{_address}/{path}" : path;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = new NetworkCredential(_username, _password);
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            Regex directoryListingRegex = new Regex(@"^([d-])((?:[rwxt-]{3}){3})\s+\d{1,}\s+.*?(\d{1,})\s+(\w+)\s+(\d{1,2})\s+(\d{4})?(\d{1,2}:\d{2})?\s+(.+?)\s?$",
                                    RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                Match match = directoryListingRegex.Match(line);

                                List<FtpItem> infoes = GetFilesListFromFtpListingUnix(line);

                                foreach (var info in infoes)
                                {
                                    FtpItem item = new FtpItem
                                    {
                                        LastModified = info.LastModified,
                                        Name = info.FileName,
                                        Permissions = info.Permissions,
                                        Size = info.FileSize,
                                        FullPath = $"{path}/{match.Groups[8].Value.Replace(" ", "%20")}",
                                    };

                                    var dir = new FtpDirectory(item);
                                    if (match.Groups[1].Value == "d")
                                    {
                                        dir.SubItems = new List<FtpItem>(RetrieveDirectoryListingsFromFtp(dir.FullPath));
                                        results.Add(dir);
                                    }
                                    else
                                    {
                                        var file = new FtpFile(item);
                                        results.Add(file);
                                    }

                                    ;
                                }
                            }
                        }
                }
            }

            return results;
        }

        public static List<FtpItem> GetFilesListFromFtpListingUnix(String filesListing)
        {
            List<FtpItem> files = new List<FtpItem>();
            MatchCollection matches = m_FtpListingRegex.Matches(filesListing);
            if (matches.Count == 0 && filesListing.Trim('\r', '\n', '\t', ' ').Length != 0)
                return null;
            foreach (Match match in matches)
            {
                FtpItem fileInfo = new FtpItem();
                char dirchar = match.Groups[1].Value.ToLowerInvariant()[0];
                fileInfo.IsDirectory = dirchar == 'd';
                fileInfo.Permissions = match.Groups[2].Value.ToCharArray();
                fileInfo.NrOfInodes = int.TryParse(match.Groups[3].Value, out var inodes) ? inodes : 1;
                fileInfo.User = match.Groups[4].Success ? match.Groups[4].Value : null;
                fileInfo.Group = match.Groups[5].Success ? match.Groups[5].Value : null;
                int.TryParse(match.Groups[6].Value, out var fileSize);
                fileInfo.FileSize = fileSize;
                string month = match.Groups[7].Value;
                string day = match.Groups[8].Value.PadLeft(2, '0');
                string year = match.Groups[9].Success ? match.Groups[9].Value : DateTime.Now.Year.ToString(CultureInfo.InvariantCulture);
                string time = match.Groups[10].Success ? match.Groups[10].Value.PadLeft(5, '0') : "00:00";
                string timeString = month + " " + day + " " + year + " " + time;
                if (!DateTime.TryParseExact(timeString, TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var lastModifiedDate))
                    lastModifiedDate = DateTime.MinValue;
                fileInfo.LastModified = lastModifiedDate;
                fileInfo.FileName = match.Groups[11].Value;
                files.Add(fileInfo);
            }
            return files;
        }
    }
}